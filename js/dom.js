// Note:
// Rembemer at this point we are talking about nodes, the correct term to refer to how JavaScript see the HTML tags.

// 1. Get node by ID
// It only return 1 result, thus never put more than 1 id in the same page.
const mainText = document.getElementById("most-important-text");
mainText.innerHTML = "Im hacking with JS! 👾";

// 2. Get node by class
// - You create an array with all the tags that have the same class.
// - Then you pick one of the elements from the array.
const arrayOfSecondaryTexts = document.getElementsByClassName("secondary-text");
const firstSecondaryText = arrayOfSecondaryTexts[0];
console.log(arrayOfSecondaryTexts);
firstSecondaryText.innerHTML = "Im hacking with JS! 👾";

// 3. Get node by tag
// - You create an array with all the tags found in the page. Even if they belong to different sections of the site.
// - Then you pick one of the elements from the array.
const arrayOfTags = document.getElementsByTagName("li");
const fourthListItem = arrayOfTags[3];
console.log(arrayOfTags);
fourthListItem.innerHTML = "Im hacking with JS! 👾";
