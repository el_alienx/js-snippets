// 1. Get HTML elements
const inputLocal = document.getElementById("inputLocal");
const buttonLocal = document.getElementById("buttonLocal");

const inputSession = document.getElementById("inputSession");
const buttonSession = document.getElementById("buttonSession");

// 2.- Event listener
buttonLocal.addEventListener("click", () => {
  saveInformation("local");
});
buttonSession.addEventListener("click", () => {
  saveInformation("session");
});

// 3.- Function
function saveInformation(type) {
  if (type === "local") localStorage.setItem("movie", inputLocal.value);
  if (type === "session") sessionStorage.setItem("song", inputSession.value);
}

function loadInformation() {
  console.log("Load info using web storage", localStorage.getItem("movie"));

  inputLocal.value = localStorage.getItem("movie");
  inputSession.value = sessionStorage.getItem("song");
}

// Call the load as soon as we can
loadInformation();
