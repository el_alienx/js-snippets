// Step 1: Get the tags by ID
const localInput = document.getElementById("localInput");
const localButton = document.getElementById("localButton");

const sessionInput = document.getElementById("sesionInput");
const sessionButton = document.getElementById("sesionButton");

// Step 2: Handle the buttons press events
localButton.addEventListener("click", () => saveInfo("local"));
sessionButton.addEventListener("click", () => saveInfo("sesion"));

// Step 3: Function to be called or "triggered" when the user press the button
function saveInfo(mode) {
  alert(`Stored ${mode} ${sessionInput.value}`);

  if (mode == "local") localStorage.setItem("movie", localInput.value);
  if (mode == "sesion") sessionStorage.setItem("song", sessionInput.value);
}

// Step 4: Load the info each time you enter the page
function loadInfo() {
  localInput.value = localStorage.getItem("movie");
  sessionInput.value = sessionStorage.getItem("song");
}

loadInfo();
